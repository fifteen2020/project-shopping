​                            ![伍蒙](https://gitee.com/fifteen2020/img-list/raw/master/img/20220103200157.svg)

#  伍蒙服装电商系统



![Vue](https://img.shields.io/badge/Vue-2.x-green.svg)  ![color](https://img.shields.io/badge/color-#5EDFD6-#5EDFD6.svg)  ![Fifteen](https://img.shields.io/badge/author-Fifteen-blue.svg)  ![spring-boot-starter-web](https://img.shields.io/badge/SpringBootWeb-2.3.10-#FCF26B.svg)

#### 1、项目最初

##### 1.1 背景

导师领进门，修行在个人

##### 1.2 理念

劳动获取面包！

##### 1.3 目标

面包自由!!!

#### 2、项目日志

| 时间 |   事件   |   备注   |
| :--: | :------: | :------: |
| 11/5 | 项目启动 | 梦的开始 |
|      |          |          |
|      |          |          |
|      |          |          |